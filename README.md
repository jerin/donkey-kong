Donkey-Kong
===

Pygame(?) version of the classic donkey-kong game. Developed as part of coursework in Spring '15.

## Game Manual
The following is the symbol table:

Symbol | Meaning
------ | -------
 P | Player
 C | Coins
 H | Stairs
 O | Fireballs
 Q | Princess
 D | Donkey
 X | Walls


The objective of the game is to rescue the princess from Donkey Kong who abducted her, and is all set to kill you with his traps.
