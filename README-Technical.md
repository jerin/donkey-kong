Implementation Details 
===

The following newly defined classes exist in the program as of now:

- `movable` -> `player`, `donkey`, `fireball`
- `board`

`movable` serves as a base class for all objects which exhibit motion in the game. The objects `player`, `donkey`, `fireball`, which are able to move inherits from this base class. Polymorphism is shown in the functions move, as the motion of player, donkey and fireball are different.

Board has a grid representing the maze though which the player has to navigate. It also contains instances of a player and donkey as of now.

Pygame is used for the graphical frontend, and smooth animations of the motion of movable objects.


The idea of Player and Donkey inheriting from Person as specified in the Assignment PDF has been ditched, as I couldn't make any sense out of the abstraction specified.
